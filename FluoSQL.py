# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
__version__ = "1.2.0"

import hashlib
import os

#AUTOLOAD by LAGVIDILO
class autoload(object):
	def cherche(self,dir):
	    FichList = [ f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir,f)) ]
	    return FichList

	def read_file(self,file):
	    f = open(file,"r")
	    R=f.read()
	    f.close()
	    return R

	def load_html(self,dir="pages"):
	    FL = self.cherche(dir)
	    R={}
	    for f in FL:
	        if f.split('.')[1]=="html":
	            BUFF = self.read_file(dir+"/"+f)
	            R[f.split('.')[0]] = BUFF
	    return R

	def load_tpl(self,dir="pages"):
	    FL = self.cherche(dir)
	    R={}
	    for f in FL:
	        if f.split('.')[1]=="tpl":
	            BUFF = self.read_file(dir+"/"+f)
	            R[f.split('.')[0]] = BUFF
	    return R


class Fluo(object):
	def __init__(self,cookies_username='auth_login',IP="localhost",port=8000):
		from bottle import route, run, template, get, post, request, response, static_file, redirect
		# AUTOLOAD PAR CYRIL COELHO
		autoload_ = autoload()
		self.pages = autoload_.load_html()
		self.tpl = autoload_.load_tpl()
		self.BUFF=""
		#self.pages={}
		self.FSQL=FluoSQL()
		self.IP,self.port = IP,port

	def buff_add(self,line):
		self.BUFF=self.BUFF+line+"\n"

	def make_simple_get(self):
		for key,i in self.pages.iteritems():
			self.buff_add("@get('/"+key+"')")
			self.buff_add("def "+key+"_html():")
			self.buff_add("	return '"+self.pages[key].replace("\n","")+"'")

		self.buff_add("run(host='"+self.IP+"', port="+str(self.port)+")")

	def init_buffer(self):
		self.BUFF=""

	def execute(self):
		from bottle import route, run, template, get, post, request, response, static_file, redirect

		#print "=====\n",self.BUFF,"\n====="
		exec(self.BUFF)
		self.BUFF = ""

import hashlib
def hash_(passwd):
	passh = hashlib.sha224(passwd).hexdigest()
	#print "1-",passwd,"-";passh
	return passh

import ast
class FluoConfig(object):
	def __init__(self,name_file="main.config"):
		self.name_file = name_file
		self.data = self.load()
		if len(self.data)==0:
			self.data={}
			self.save(self.data)

	def save(self,data):
		f = open(self.name_file,'w')
		f.write(str(data))
		f.close()

	def load(self):
		try:
			f = open(self.name_file,'r')
			data = f.read()
			if len(data)>0:
				data = ast.literal_eval(data)
			f.close()
			return data
		except IOError:
			f = open(self.name_file,'w')
			f.write("{}")
			f.close()
			return {}

	def load_current_data(self):
		self.data = self.load()

	def get_all(self):
		self.load_current_data()
		return self.data

	def get_one(self,key):
		try:
			return self.data[key]
		except:
			return None

	def set_all(self,data):
		self.save(data)

	def set_one(self,key,value):
		self.data[key] = value
		self.save(self.data)
		

import httplib, urllib
class FluoSQL(object):
	def __init__(self,IP="128.79.156.73",user="phpmyadmin",password="6345",base="Exemple",http=False,secure={},phone="",conn_root="/CATIAERP/connect",req_root="/CATIAERP/req"):
		self.online = {'user':None, 'hash':None}
		self.conn_root,self.req_root = conn_root,req_root
		self.conn,self.cursor = None,None
		self.IP=IP
		self.username,self.passhash = user,password
		self.secure,self.phone=secure,phone
		self.headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
		if not http: 
			self.db_connect(IP,user,password,base)
		"""
		if signup:
			if http: self.http_connect(IP,user,hash_(password),secure,phone)
		else:	
			if http: self.http_connect(IP,user,hash_(password))
		"""
		self.__REQUEST__ = ""
		

	def db_connect(self,IP,user,password,base):#host="192.168.1.25",user="invite",password="1234", database="licorne"
		import mysql.connector
		self.conn = mysql.connector.connect(host=IP,user=user,password=password, database=base)#sqlite3.connect(link) #invite 1234
		self.conn.text_factory = str
		self.cursor = self.conn.cursor(buffered=True,dictionary=True)

	def db_close(self):
		self.conn.commit()
		self.conn.close()

	def http_connect(self,signup=False):
		phone=self.phone
		secure=self.secure
		IP=self.IP
		username,passhash=self.username,hash_(self.passhash) 
		
		if signup:
			params = urllib.urlencode({'@mode': 'new', '@username':username, '@hash':passhash, "@secure":secure, "@phone":phone})
		else:
			params = urllib.urlencode({'@mode': 'connect', '@username':username, '@hash':passhash})
		#print "ETAT: ",params	
		
		self.conn_http = httplib.HTTPConnection(self.IP)
		self.conn_http.connect()
		self.conn_http.request("POST", self.conn_root, params, self.headers)
		response = self.conn_http.getresponse()
		#print response.status, response.reason
		data = response.read()
		#print data
		return data	


	def send_request(self):
		#print "REQUEST SEND:",self.__REQUEST__
		params = urllib.urlencode({'@req': self.__REQUEST__, '@username':self.username, '@hash':hash_(self.passhash)})
		self.conn_http = httplib.HTTPConnection(self.IP)
		self.conn_http.connect()
		self.conn_http.request("POST", self.req_root, params, self.headers)
		response = self.conn_http.getresponse()
		#print response.status, response.reason
		data = response.read()
		#print data
		return data	


	def http_close(self):
		conn.close()

	def request_init(self):
		self.__REQUEST__ = ""

	def request_add(self,TMP):
		self.__REQUEST__=self.__REQUEST__+TMP

	def set_request(self,request,mode=0):
		self.request_init()
		self.__REQUEST__ = request
		type_request = self.__REQUEST__.split(' ')[0]
		self.cursor.execute(self.__REQUEST__)
		if type_request in "UPDATE INSERT DELETE": self.conn.commit()

		
		if type_request == "SELECT":
			RET = None
			if mode==1 or mode=="one":
				RET= self.cursor.fetchone()
			if mode==0 or mode=="all":
				RET= self.cursor.fetchall()
			if mode==-1:
				for row in self.cursor:
					RET = row[0]
					break

			return RET

	def get_reponse(self,mode=1):
		if "SELECT" in self.__REQUEST__:
			RET = None
			if mode==1 or mode=="one":
				RET= self.cursor.fetchone()
			if mode==0 or mode=="all":
				RET= self.cursor.fetchall()
			if mode==-1:
				for row in self.cursor:
					RET = row[0]
					break

			return RET

	def make_simple_request(self, type_request = "select", selected_row="*", values="", selected_from="Auth", WHERE={}, mode=0, set_current=False, inc=False):
		self.request_init()
		type_request=type_request.upper()
		if type_request=="DELETE":
			self.request_add("DELETE FROM ")
			self.request_add(selected_from+" ")
			for key in WHERE:
				try:
					K = str(int(WHERE[key]))
				except:
					K = '\"'+str(WHERE[key])+'\"'
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+""+K+" ")
					else:
						self.request_add("WHERE "+key+"="+K+" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+""+K+" ")
					else:
						self.request_add("AND "+key+"="+K+" ")



		if type_request=="INSERT":
			self.request_add("INSERT INTO ")
			self.request_add(selected_from+" (")
			if type(selected_row)!= list: selected_row=selected_row.split(',')
			if type(values)!= list: values=values.split(',')
			for i in range(0,len(selected_row)):
				self.request_add(selected_row[i])
				if i<len(selected_row)-1: self.request_add(", ")
			self.request_add(") VALUES(")
			for i in range(0,len(values)):
				#print "I:",i,"values[i]:",values[i]
				if (type(values[i]) == int) or (type(values[i]) == float):
					self.request_add(str(values[i]))
				else:
					self.request_add("\""+values[i]+"\"")
				if i<len(values)-1: self.request_add(", ")
			self.request_add(')')


		if type_request=="UPDATE":
			self.request_add("UPDATE ")
			self.request_add(selected_from+" SET "+selected_row+" ")
			if type(selected_row)!= list:selected_row=selected_row.split(',')
			if type(values)!= list:values=values.split(',')
			for i in range(0,len(selected_row)):
				if (type(values[i]) == int) or (type(values[i]) == float):
					self.request_add("="+str(values[i]))
				else:
					self.request_add("=\""+values[i]+"\"")

				if i<len(selected_row)-1: self.request_add(", ")
			self.request_add(" ")
			for key in WHERE:
				try:
					K = str(int(WHERE[key]))
				except:
					K = '\"'+str(WHERE[key])+'\"'
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+""+K+" ")
					else:
						self.request_add("WHERE "+key+"="+K+" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+""+K+" ")
					else:
						self.request_add("AND "+key+"="+K+" ")


		if type_request=="SELECT":
			self.request_add("SELECT ")
			self.request_add(selected_row+" ")
			self.request_add("FROM "+selected_from+" ")

			for key in WHERE:
				try:
					K = str(int(WHERE[key]))
				except:
					K = '\"'+str(WHERE[key])+'\"'
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+""+K+" ")
					else:
						self.request_add("WHERE "+key+"="+K+" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+""+K+" ")
					else:
						self.request_add("AND "+key+"="+K+" ")

		#print "self.__REQUEST__:",self.__REQUEST__

		if type_request not in "INSERT UPDATE": print "\n\nSQL:" ,self.__REQUEST__,"\n\n"
		##print "EXECUTE:","self.cursor.execute(self.__REQUEST__)"
		if not set_current:
			self.cursor.execute(self.__REQUEST__)
			if type_request in "UPDATE INSERT DELETE": self.conn.commit()
			RET = True
			self.request_init()
			if type_request == "SELECT":
				
				if mode==1 or mode=="one":
					RET= self.cursor.fetchone()
				if mode==0 or mode=="all":
					RET= self.cursor.fetchall()
				if mode==-1:
					for row in self.cursor:
						RET = row[0]
						break
			return RET
		else:
			if mode!=-1:
				return self.__REQUEST__
		








