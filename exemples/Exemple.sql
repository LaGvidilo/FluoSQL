-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 02 Octobre 2017 à 13:09
-- Version du serveur :  10.1.23-MariaDB-9+deb9u1
-- Version de PHP :  5.6.22-2+b3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Exemple`
--

-- --------------------------------------------------------

--
-- Structure de la table `Auth`
--

CREATE TABLE `Auth` (
  `id` int(5) NOT NULL,
  `email` text,
  `HASH` text,
  `secure` text,
  `phone` text,
  `blocked` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Auth`
--

INSERT INTO `Auth` (`id`, `email`, `HASH`, `secure`, `phone`, `blocked`) VALUES
(1, 'name@mail.com', '99fb2f48c6af4761f904fc85f95eb56190e5d40b1f44ec3a9c1fa319', '{\'Name of city is your born\': \'Paris\'}', '+33640102014', 0);

-- --------------------------------------------------------

--
-- Structure de la table `stats`
--

CREATE TABLE `stats` (
  `site` text,
  `visit` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `stats`
--

INSERT INTO `stats` (`site`, `visit`) VALUES
('licorne', 673),
('pub1', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Auth`
--
ALTER TABLE `Auth`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Auth`
--
ALTER TABLE `Auth`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
