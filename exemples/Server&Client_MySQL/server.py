#coding: utf-8
from FluoSQL import FluoSQL as FSQL
SQL_base = FSQL(IP="192.168.1.21",user="admin",password="1234",base="Exemple")#For make request simple
SQL_common = FSQL(IP="192.168.1.21",user="admin",password="1234",base="Exemple")#For send request


#SERVEUR FluoSQL======================================START
from bottle import route, run, template, get, post, request, response, static_file, redirect

@post('/connect')
def connect_fluo():
	mode = request.POST['@mode']
	username = request.POST['@username']
	passhash = request.POST['@hash']
	if mode == "connect":
		print "\n/connect"
		SQL_base.make_simple_request(type_request = "select", selected_row="email, HASH, blocked", selected_from="Auth", WHERE={'email':username,'HASH':passhash}, set_current=True)
		print "REQUEST:",SQL_base.__REQUEST__
		RES = SQL_common.set_request(SQL_base.__REQUEST__,1)
		print "RESULT:",RES
		return str(RES)
	else:
		return None

@post('/req')
def request_fluo():
	username = request.POST['@username']
	passhash = request.POST['@hash']
	securelogin=False
	SQL_base.make_simple_request(type_request = "select", selected_row="email, HASH, blocked", selected_from="Auth", WHERE={'email':username,'HASH':passhash}, set_current=True)
	print "SQL_base.__REQUEST__",SQL_base.__REQUEST__,'\n'
	RES = SQL_common.set_request(SQL_base.__REQUEST__,1)
	try:
		u,h,b = RES['email'],RES['HASH'],RES['blocked']
		if b==0:
			if u == username: 
				if h == passhash:
					securelogin=True
	except:
		print "Pas connecté"

	if (securelogin==False): 
		return "<AUTH_ERROR>"
	else:
		Req = str(request.POST['@req'])+';'
		print "\n/req"
		print "(",Req,")"
		try:
			print "REQUEST:",Req
			print "RESULT:",str(SQL_common.set_request(Req))
			return str(SQL_common.set_request(Req))
		except:
			return "<SQL_ERROR>"
			
run(host='0.0.0.0', port=8000)
#SERVEUR FluoSQL======================================END
