# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding("utf-8")


import hashlib
import os

#AUTOLOAD by LAGVIDILO
class autoload(object):
	def cherche(self,dir):
	    FichList = [ f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir,f)) ]
	    return FichList

	def read_file(self,file):
	    f = open(file,"r")
	    R=f.read()
	    f.close()
	    return R

	def load_html(self,dir="pages"):
	    FL = self.cherche(dir)
	    R={}
	    for f in FL:
	        if f.split('.')[1]=="html":
	            BUFF = self.read_file(dir+"/"+f)
	            R[f.split('.')[0]] = BUFF
	    return R

	def load_tpl(self,dir="pages"):
	    FL = self.cherche(dir)
	    R={}
	    for f in FL:
	        if f.split('.')[1]=="tpl":
	            BUFF = self.read_file(dir+"/"+f)
	            R[f.split('.')[0]] = BUFF
	    return R

# AUTOLOAD PAR CYRIL COELHO
autoload ="" 
pages =""
tpl =""


class Fluo(object):
	def __init__(self,cookies_username='auth_login'):
		from bottle import route, run, template, get, post, request, response, static_file, redirect
		# AUTOLOAD PAR CYRIL COELHO
		autoload = autoload()
		pages = autoload.load_html()
		tpl = autoload.load_tpl()
		self.BUFF=""
		self.pages={}
		self.FSQL=FluoSQL()

	def buff_add(self,line):
		self.BUFF=self.BUFF+line+"\n"

	def make_simple_get(self):
		for key,i in pages.iteritems():
			self.buff_add("@get('/"+key+"')")
			self.buff_add("def "+key+"_html():")
			self.buff_add("	return '"+pages[key].replace("\n","")+"'")
			@get('/')
			def index():
				return pages['index']

	def init_buffer(self):
		self.BUFF=""

	def execute(self):
		from bottle import route, run, template, get, post, request, response, static_file, redirect

		#print "=====\n",self.BUFF,"\n====="
		exec(self.BUFF)
		self.BUFF = ""

import hashlib
def hash_(passwd):
	passh = hashlib.sha224(passwd).hexdigest()
	#print "1-",passwd,"-";passh
	return passh

import httplib, urllib
class FluoSQL(object):
	def __init__(self,IP="192.168.1.31",user="phpmyadmin",password="6345",base="afroweb",http=False,IP_http="192.168.1.31"):
		self.conn,self.cursor = None,None
		if not http: 
			self.db_connect(IP,user,password,base)
		if http: self.http_connect(IP,user,hash_(password))
		self.__REQUEST__ = ""
		self.online = {'user':None, 'hash':None}

	def db_connect(self,IP,user,password,base):#host="192.168.1.25",user="invite",password="1234", database="licorne"
		import mysql.connector
		self.conn = mysql.connector.connect(host=IP,user=user,password=password, database=base)#sqlite3.connect(link) #invite 1234
		self.conn.text_factory = str
		self.cursor = self.conn.cursor(buffered=True,dictionary=True)

	def db_close(self):
		self.conn.commit()
		self.conn.close()

	def http_connect(self,IP, username, passhash):
		self.IP=IP
		self.username,self.passhash = username,passhash
		params = urllib.urlencode({'@mode': 'connect', '@username':username, '@hash':passhash})
		self.headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
		self.conn = httplib.HTTPConnection(self.IP+":8000")
		self.conn.request("POST", "/connect", params, self.headers)
		response = self.conn.getresponse()
		print response.status, response.reason
		data = response.read()
		print data
		self.online = {'user':data}		

	def send_request(self):
		print "REQUEST SEND:",self.__REQUEST__
		params = urllib.urlencode({'@req': self.__REQUEST__, '@username':self.username, '@hash':self.passhash})
		self.conn.request("POST", "/req", params, self.headers)
		response = self.conn.getresponse()
		print response.status, response.reason
		data = response.read()
		print data
		return data	


	def http_close(self):
		conn.close()

	def request_init(self):
		self.__REQUEST__ = ""

	def request_add(self,TMP):
		self.__REQUEST__=self.__REQUEST__+TMP

	def set_request(self,request,mode=0):
		self.request_init()
		self.__REQUEST__ = request
		type_request = self.__REQUEST__.split(' ')[0]
		self.cursor.execute(self.__REQUEST__)
		if type_request in "UPDATE INSERT DELETE": self.conn.commit()

		
		if type_request == "SELECT":
			RET = None
			if mode==1 or mode=="one":
				RET= self.cursor.fetchone()
			if mode==0 or mode=="all":
				RET= self.cursor.fetchall()
			if mode==-1:
				for row in self.cursor:
					RET = row[0]
					break

			return RET

	def get_reponse(self,mode=1):
		if "SELECT" in self.__REQUEST__:
			RET = None
			if mode==1 or mode=="one":
				RET= self.cursor.fetchone()
			if mode==0 or mode=="all":
				RET= self.cursor.fetchall()
			if mode==-1:
				for row in self.cursor:
					RET = row[0]
					break

			return RET

	def make_simple_request(self, type_request = "select", selected_row="*", values="", selected_from="Auth", WHERE={}, mode=0, set_current=False, inc=False):
		self.request_init()
		type_request=type_request.upper()
		if type_request=="DELETE":
			self.request_add("DELETE FROM ")
			self.request_add(selected_from+" ")
			for key in WHERE:
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+"\""+str(WHERE[key])+"\" ")
					else:
						self.request_add("WHERE "+key+"=\""+str(WHERE[key])+"\" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+"\""+str(WHERE[key])+"\" ")
					else:
						self.request_add("AND "+key+"=\""+str(WHERE[key])+"\" ")



		if type_request=="INSERT":
			self.request_add("INSERT INTO ")
			self.request_add(selected_from+" (")
			if type(selected_row)!= list: selected_row=selected_row.split(',')
			if type(values)!= list: values=values.split(',')
			for i in range(0,len(selected_row)):
				self.request_add(selected_row[i])
				if i<len(selected_row)-1: self.request_add(", ")
			self.request_add(") VALUES(")
			for i in range(0,len(values)):
				self.request_add("\""+values[i]+"\"")
				if i<len(values)-1: self.request_add(", ")
			self.request_add(')')


		if type_request=="UPDATE":
			self.request_add("UPDATE ")
			self.request_add(selected_from+" SET ")
			if type(selected_row)!= list:selected_row=selected_row.split(',')
			if type(values)!= list:values=values.split(',')
			for i in range(0,len(selected_row)):
				if inc:
					self.request_add(selected_row[i]+"="+selected_row[i]+"+1")
				else: 
					self.request_add(selected_row[i]+"=\""+values[i]+"\"")
				if i<len(selected_row)-1: self.request_add(", ")
			self.request_add(" ")
			for key in WHERE:
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+"\""+str(WHERE[key])+"\" ")
					else:
						self.request_add("WHERE "+key+"=\""+str(WHERE[key])+"\" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+"\""+str(WHERE[key])+"\" ")
					else:
						self.request_add("AND "+key+"=\""+str(WHERE[key])+"\" ")


		if type_request=="SELECT":
			self.request_add("SELECT ")
			self.request_add(selected_row+" ")
			self.request_add("FROM "+selected_from+" ")

			for key in WHERE:
				print "WHERE: ",key
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+"\""+str(WHERE[key])+"\" ")
					else:
						self.request_add("WHERE "+key+"=\""+str(WHERE[key])+"\" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+"\""+str(WHERE[key])+"\" ")
					else:
						self.request_add("AND "+key+"=\""+str(WHERE[key])+"\" ")


		##print "SQL:" ,self.__REQUEST__
		##print "EXECUTE:","self.cursor.execute(self.__REQUEST__)"
		if not set_current:
			self.cursor.execute(self.__REQUEST__)
			if type_request in "UPDATE INSERT DELETE": self.conn.commit()

			self.request_init()
			if type_request == "SELECT":
				RET = None
				if mode==1 or mode=="one":
					RET= self.cursor.fetchone()
				if mode==0 or mode=="all":
					RET= self.cursor.fetchall()
				if mode==-1:
					for row in self.cursor:
						RET = row[0]
						break
				return RET
		else:
			if mode!=-1:
				return self.__REQUEST__
		








